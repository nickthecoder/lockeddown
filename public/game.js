// Patch missing URLSearchParams. See https://stackoverflow.com/questions/45758837/script5009-urlsearchparams-is-undefined-in-ie-11
(function (w) {
    w.URLSearchParams = w.URLSearchParams || function (searchString) {
        var self = this;
        self.searchString = searchString;
        self.get = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(self.searchString);
            if (results == null) {
                return null;
            } else {
                return decodeURI(results[1]) || 0;
            }
        };
    }
})(window)

var config = {
    type: Phaser.AUTO,
    parent: 'gameContainer',
    width: 800,
    height: 600,
    scene: {
        preload: preload,
        create: create,
        update: update
    } 
};
 
var game = new Phaser.Game(config);
var scene;

var playerNumber = 0;
var isSpectator = true;
var chatVolume = 'loud'; // All chat is displayed. 'quiet' (players only) 'silent' (no chat at all).

var assets = {};
var pieces = {};
var buttonInfos = {}; // Hash of button data keyed on button.name.
var revealAreas = {};
var snapAreas = {};
var sounds = {};
var pointerHighlights = {};
var pieceCounters = {}; // Keyed on the "hash" of 'x,y' (as a string).
var muted = false;

// The name of the piece being dragged.
// Reset to null if another player also dragged the piece (and got to the server first).
var draggingPieceName = null;

function preload() {
    console.log( "Preload" );

    // Nine patch plug-in for name tabgs (and other future labels).
    this.load.plugin('rexninepatchplugin', 'rexninepatchplugin.min.js', true);
                      
    this.load.audio( 'goGoGo', [ 'assets/sounds/goGoGo.ogg', 'assets/sounds/goGoGo.mp3' ]);
    this.load.audio( 'goAtOnce', [ 'assets/sounds/goAtOnce.ogg', 'assets/sounds/goAtOnce.mp3' ]);
    this.load.audio( 'lol', [ 'assets/sounds/lol.ogg', 'assets/sounds/lol.mp3' ]);
    this.load.audio( 'dingDong', [ 'assets/sounds/dingDong.ogg', 'assets/sounds/dingDong.mp3' ]);
    this.load.audio( 'dongDing', [ 'assets/sounds/dongDing.ogg', 'assets/sounds/dongDing.mp3' ]);
    this.load.audio( 'ping', [ 'assets/sounds/ping.ogg', 'assets/sounds/ping.mp3' ]);
    this.load.audio( 'up', [ 'assets/sounds/up.ogg', 'assets/sounds/up.mp3' ]);
    this.load.audio( 'down', [ 'assets/sounds/down.ogg', 'assets/sounds/down.mp3' ]);
    this.load.audio( 'no', [ 'assets/sounds/no.ogg', 'assets/sounds/no.mp3' ]);
    this.load.audio( 'yes', [ 'assets/sounds/yes.ogg', 'assets/sounds/yes.mp3' ]);
    this.load.audio( 'reveal', [ 'assets/sounds/reveal.ogg', 'assets/sounds/reveal.mp3' ]);
    
    this.load.image( 'spark', 'assets/spark.png' );
    // Most assets are loaded dynamically when the server emits an 'assets' message
}

function create() {
    console.log( "Create" );
    scene = this;
    var self = this;
    
    this.highlightPointerKey = this.input.keyboard.addKey( Phaser.Input.Keyboard.KeyCodes.CTRL );
    
    this.particles = this.add.particles('spark');
    this.particles.depth = 200000;

    sounds[ 'go' ] = [ this.sound.add('goGoGo'), this.sound.add('goAtOnce') ];
    sounds[ 'lol' ] = [ this.sound.add('lol') ];
    sounds[ 'Connected' ] = [ this.sound.add('dingDong') ];
    sounds[ 'Disconnected' ] = [ this.sound.add('dongDing') ];
    sounds[ 'Ping' ] = [ this.sound.add('ping') ];
    sounds[ 'up' ] = [ this.sound.add('up') ];
    sounds[ 'down' ] = [ this.sound.add('down') ];
    sounds[ 'no' ] = [ this.sound.add('no') ];
    sounds[ 'yes' ] = [ this.sound.add('yes') ];
    sounds[ 'reveal' ] = [ this.sound.add('reveal') ];

    this.input.on('dragstart', function (pointer, piece) {
        if (!isSpectator) {
            draggingPieceName = piece.name;
            if (piece.gluedTo) {
                pieces[piece.gluedTo].setTint(0x00ff00);
            } else {
                piece.setTint(0x00ff00);
            }
        }
    });

    this.input.on('drag', function (pointer, piece, dragX, dragY) {
        if (!isSpectator && draggingPieceName) {
            // The piece should move when the server send back a 'movePiece' message.
            updatePieceCounters(piece.x, piece.y, dragX, dragY);
            piece.x = dragX;
            piece.y = dragY;
            snap( piece, piece );
            piece.setDepth(1000000);

            if (piece.gluedTo) {
                const other = pieces[ piece.gluedTo ];
                self.socket.emit('draggingPiece', { end : false, name : piece.gluedTo, x: piece.x, y: piece.y } );
                updatePieceCounters(piece.x, piece.y, dragX, dragY);
                other.x = piece.x;
                other.y = piece.y;
                other.setDepth(999999);
            } else {
                self.socket.emit('draggingPiece', { end : false, name : piece.name, x: piece.x, y: piece.y } );
                const other = findGluedTo( piece );
                if (other) {
                    updatePieceCounters(other.x, other.y, piece.x, piece.y);
                    other.x = piece.x;
                    other.y = piece.y;
                    other.setDepth(1000001);
                }
            }
        }
    });

    this.input.on('dragend', function (pointer, piece) {
        if (!isSpectator && draggingPieceName) {
            if (piece.gluedTo) {
                pieces[piece.gluedTo].clearTint();
                self.socket.emit('draggingPiece', { end : true, name : piece.gluedTo, x: piece.x, y: piece.y } );
            } else {
                piece.clearTint();
                self.socket.emit('draggingPiece', { end : true, name : piece.name, x: piece.x, y: piece.y } );
            }
        }
    });
    
    // Highlight the pointer by holding down the ctrl key.
    this.input.on('pointermove', function (pointer) {
        if (!isSpectator) {
            if (self.highlightPointerKey.isDown) {
                self.socket.emit('highlightPointer', {
                    playerNumber: playerNumber,
                    isSpectator: isSpectator,
                    on: true,
                    x: pointer.x,
                    y : pointer.y
                    
                });
            }
        }
    });
    this.highlightPointerKey.on('down', function(key,event) {
        if (!isSpectator) {
            var pointer= self.input;
            self.socket.emit('highlightPointer', { playerNumber: playerNumber, on: true,x: pointer.x, y : pointer.y } );
        }
    });
    this.highlightPointerKey.on('up', function(key,event) {
        if (!isSpectator) {
            self.socket.emit('highlightPointer', { playerNumber: playerNumber, on: false, x: -1000, y : -1000 } );
        }
    });
    document.addEventListener("visibilitychange", function() {
        if (!isSpectator) {
            self.socket.emit('highlightPointer', { playerNumber: playerNumber, on: false, x: -1000, y : -1000 } );
        }
    });
    
    this.socket = io();
    const gameId = new URLSearchParams(window.location.search).get( 'gameId' );
    document.title = gameId;
    this.socket.emit( 'game', { gameId : gameId } );
    
    this.socket.on('gameInfo', function (gameInfo) {
        document.title = gameInfo.name;
        displayHTMLMessage( 'Type <b>help</b> in the <i>Chat</i> box below to list the special chat commands.' );
        displayHTMLMessage( '<br/>' );
        displayHTMLMessage( 'There are ' + (gameInfo.playerCount-1) + ' other players in this game.' );
        displayHTMLMessage( '<br/>' );
        if (gameInfo.instructions) {
            displayHTMLMessage( gameInfo.instructions );
            displayHTMLMessage( '<br/>' );
        }
    });
    
    this.socket.on('playerInfo', function (playerInfo) {
        playerNumber = playerInfo.playerNumber;
        isSpectator = playerInfo.isSpectator;
    });

    this.socket.on('assets', function (assets) {
        Object.keys(assets).forEach(function (id) {
            addAsset(self, id, assets[id]);
        });
        self.load.start();
    });

    this.socket.on('specialAreas', function (specialAreas) {
        Object.keys(specialAreas).forEach(function (id) {
            addSpecialArea(self, id, specialAreas[id]);
        });
    });
    
    this.socket.on('backgrounds', function (backgrounds) {
        postLoad( self, function() {
            Object.keys(backgrounds).forEach(function (id) {
                addBackground(self, backgrounds[id]);
            });
        });
    });
    
    this.socket.on('pieces', function (pieces) {
        postLoad( self, function() {
            const wasMuted = muted;
            muted=true;
            clearPieces(self);
            Object.keys(pieces).forEach(function (id) {
                addPiece(self, pieces[id]);
            });
            muted=wasMuted;
        });
    });
    
    this.socket.on('addPieces', function (pieces) {
        postLoad( self, function() {
            const wasMuted = muted;
            muted=true;
            Object.keys(pieces).forEach(function (id) {
                addPiece(self, pieces[id]);
            });
            muted=wasMuted;
        });
    });
    
    this.socket.on('movePiece', function(moveInfo) {
        movePiece( self, moveInfo );
    });
    
    this.socket.on('movePieces', function (piecesInfo) {
        postLoad( self, function() {
            Object.keys(piecesInfo).forEach(function (id) {
                var pieceInfo = piecesInfo[id];
                movePiece(self, pieceInfo);
            });
        });
    });
    
    this.socket.on('throwDice', function (piecesInfo) {
        postLoad( self, function() {
            Object.keys(piecesInfo).forEach(function (id) {
                var pieceInfo = piecesInfo[id];
                movePiece(self, pieceInfo);
                
                var piece = pieces[pieceInfo.name];
                
                self.anims.remove( piece.name );
                var diceAnimation = self.anims.create({
                    key: piece.name,
                    frames: self.anims.generateFrameNumbers(pieceInfo.assetName, {frames:[
                        getRandomInt(6),getRandomInt(6),getRandomInt(6),getRandomInt(6),getRandomInt(6),getRandomInt(6),pieceInfo.assetNumber
                    ]}),
                    frameRate: 16,
                    repeat: 0
                });
                pieces[pieceInfo.name].play(piece.name);

            });
        });
    });
    
    this.socket.on('buttons', function (buttons) {
        if ( !isSpectator ) {
            postLoad( self, function() {
                Object.keys(buttons).forEach(function (id) {
                    const button = buttons[id];
                    createButton( button );
                });
            });
        }
    });
    
    this.socket.on('highlightPointer', function(pointerInfo) {
        // If there is a friend in chat, then we want to see his pointer highlighter.
        // But if there's a troll, we want to turn it off. Reuse the 'ch' for this purpose.
        if ( showChat( pointerInfo.isSpectator) ) {
            var ph = findOrCreatePointerHighlight(pointerInfo.playerNumber);
            ph.on = pointerInfo.on;
            ph.setPosition( pointerInfo.x, pointerInfo.y );
        }
    });
    
    this.socket.on('connected', function(playerInfo) {
        // NOTE, Spectators connecting are always displayed.
        // But disconnected and changeName event obey 'chatVolume'.
        // This is because we ALWAYS want to see then a friend/troll arrives, but if a troll renamed
        // themselves to something obnoxious, then we want to be able to ignore it. 
        displayChat( playerInfo.name, 'Connected', playerInfo.playerNumber, true, playerInfo.isSpectator );
    });
    
    this.socket.on('disconnected', function(playerInfo) {
        // Ignore trolls' disconnected messages, because they may be using their names to communicate.
        if ( showChat(playerInfo.isSpectator) ) {
            displayChat( playerInfo.name, 'Disconnected', playerInfo.playerNumber, true, playerInfo.isSpectator );
            findOrCreatePointerHighlight(playerInfo.playerNumber).on=false;
        }
    });
    
    this.socket.on('changeName', function(renameInfo) {
        // Ignore trolls' disconnected messages, because they may be using their names to communicate.
        if ( showChat(renameInfo.isSpectator)  ) {
            displayChat( renameInfo.from, 'renamed to ' + (renameInfo.to), renameInfo.playerNumber, false, renameInfo.isSpectator );
        }
    });
    
    this.socket.on('chat', function(chatInfo) {
        if ( showChat(chatInfo.isSpectator) ) {
            displayChat( chatInfo.name, chatInfo.message, chatInfo.playerNumber, true, chatInfo.isSpectator );
        }
    });
    
    
    usernameInput.value = getNameCookie();
    changeName();
}

function createButton( button ) { 
    const sprite = scene.add.sprite( button.x, button.y, button.assetName );
    sprite.setInteractive();
    sprite.on('pointerdown', function (pointer) { this.setTexture( button.assetName, 1 ); });
    sprite.on('pointerover', function (pointer) { this.setTexture( button.assetName, 2 ); });
    sprite.on('pointerout', function (pointer) { this.setTexture( button.assetName, 0 ); });
    sprite.on('pointerup', function (pointer) {
        this.setTexture( button.assetName, 2 );
        if ( ! button.confirm || window.confirm( button.text + '?' ) ) {
            scene.socket.emit( 'click', { name : button.name } );
        }
    });
    const style = { fontFamily: 'Arial', fontSize: '16px', fontStyle: 'bold' };
    const text = scene.add.text( button.x, button.y, button.text, style );
    text.setPosition( button.x - text.width/2, button.y - text.height/2 -2 );
    text.setDepth(1);
    buttonInfos[button.name] = button;
}

function showChat( isSpectator ) {
    if ( chatVolume == 'loud' ) return true;
    if ( chatVolume == 'silent' ) return false;
    return !isSpectator;
}

function update() {}

function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}

function postLoad( self, action ) {
    if (self.load.isLoading()) {
        self.load.on('complete', action );
    } else {
        action();
    }
}

function findOrCreatePointerHighlight( playerNumber ) {
    var result = pointerHighlights[playerNumber];
    if (! result) {
        var circle = new Phaser.Geom.Circle(0, 0, 30);
        var color = [ 0xff0000,  0x0000ff, 0xaaaa00 , 0xee00ee, 0x00eeee, 0x00ff00][ playerNumber % 6 ];
        result = scene.particles.createEmitter({
            x: 400,
            y: 300,
            frequency: 60,
            scale: { start: 1, end: 0 },
            tint: color,
            emitZone: { type: 'edge', source: circle, quantity: 10, yoyo: false }
        });        
        pointerHighlights[playerNumber] = result;
    }

    return result;
}

function addSpecialArea(self, id, specialArea) {
    if (specialArea.type == 'reveal') {
        // Ignore reveal areas which are not for the current player.
        if ( specialArea.playerNumber == null || specialArea.playerNumber == playerNumber || specialArea.playerNumber == -1 ) {
            revealAreas[id] = new Phaser.Geom. Rectangle(
                specialArea.x,
                specialArea.y,
                specialArea.width,
                specialArea.height
            );
        }
        
    } else if (specialArea.type == 'snap') {
        snapAreas[id] = {
            rect: new Phaser.Geom. Rectangle(
                specialArea.x,
                specialArea.y,
                specialArea.width,
                specialArea.height
            ),
            snapX: specialArea.snapX,
            snapY: specialArea.snapY
        };
        
    } else if (specialArea.type == 'counter' ) {
        const key = '' + specialArea.x + ',' + specialArea.y;
        const color = specialArea.color == null ? '#fff' : specialArea.color;
        var text = scene.add.text( specialArea.textX, specialArea.textY, '', {color:color} );
        text.depth = 1;

        pieceCounters[key] = {
            x: specialArea.x,
            y: specialArea.y,
            value: 0,
            text: text
        };
    }
}

function inRevealArea( pieceInfo ) {
    for ( var id in revealAreas ) {
        const ra = revealAreas[id];
        if ( Phaser.Geom.Rectangle.Contains( ra,  pieceInfo.x, pieceInfo.y ) ) {
            return true;
        }
    }
    return false;
}

function findSnapArea( source ) {
    for ( var id in snapAreas ) {
        const area = snapAreas[id];
        if ( Phaser.Geom.Rectangle.Contains( area.rect,  source.x, source.y ) ) {
            return area;
        }
    }
}

function findPieceCounter( x, y ) {
    const key = '' + x + ',' + y;
    return pieceCounters[key];
}

function updatePieceCounters( fromX, fromY, toX, toY ) {
    if (fromX == toX && fromY == toY) {
        return;
    }
    
    var pc = findPieceCounter(fromX, fromY);

    if (pc) {
        pc.value --;
        pc.text.text = '' + pc.value;
        playNamedSound( 'down' );
    }
    pc = findPieceCounter(toX, toY);
    if (pc) {
        pc.value ++;
        pc.text.text = '' + pc.value;
        playNamedSound( 'up' );
    }
}

function snap( source, destination ) {
    const area = findSnapArea( source );
    if (area) {
        destination.x = Math.floor((source.x - area.rect.x) / area.snapX) * area.snapX + area.rect.x + area.snapX/2;
        destination.y = Math.floor((source.y - area.rect.y) / area.snapY) * area.snapY + area.rect.y + area.snapY/2;
    } else {
        destination.x = source.x;
        destination.y = source.y;
    }
}

function addAsset(self, id, assetInfo) {
    var asset = {
        name : assetInfo.name,
        url : assetInfo.url,
        sizeX : assetInfo.sizeX,
        sizeY : assetInfo.sizeY
    };
    if ( assetInfo.type == 'ninepatch' ) {
        asset.isNinePatch = true;
        asset.columns = assetInfo.columns;
        asset.rows = assetInfo.rows;
        asset.margin = assetInfo.margin;
    }
        
    assets[id] = asset;

    if ( assetInfo.type == 'spritesheet' ) {
        self.load.spritesheet( assetInfo.name, assetInfo.url, { frameWidth : assetInfo.sizeX, frameHeight : assetInfo.sizeY } );
    } else {
        self.load.image( assetInfo.name, assetInfo.url );
    }

}

function addBackground( self, backgroundInfo ) {
    var asset = assets[ backgroundInfo.assetName ];
    self.add.sprite( backgroundInfo.x, backgroundInfo.y, backgroundInfo.assetName)
        .setOrigin(0.5, 0.5)
        .setDisplaySize(asset.sizeX, asset.sizeY);
}

function clearPieces( self ) {
    Object.keys(pieces).forEach(function (id) {
        pieces[id].destroy();
    });
    pieces = {};
    for (var id in pieceCounters) {
        var pc = pieceCounters[id];
        pc.value = 0;
        pc.text.text = '0';
    }
}

function findGluedTo( piece ) {
    for ( pName in pieces ) {
        const p = pieces[pName];
        if (p.gluedTo == piece.name) {
            return p;
        }
    }
    return null;
}

function max(a, b) { return a > b ? a : b; }

function addPiece( self, pieceInfo ) {
    var text = null;
    if ( pieceInfo.text ) {
        text = self.add.text( 0,0, pieceInfo.text, pieceInfo.textStyle );
        text.setDepth(pieceInfo.zIndex + 1);
    }
    var piece = null;
    
    var asset = assets[ pieceInfo.assetName ];

    if ( text && asset.isNinePatch ) {
        piece = scene.add.rexNinePatch({
            x: pieceInfo.x,
            y: pieceInfo.y,
            width: asset.width,
            height: asset.height,
            key: 'nameTag',
            columns: asset.columns,
            rows: asset.rows,
        });
        piece.asset = asset;
        piece.setOrigin(0.5, 0.5);
        piece.setDepth(pieceInfo.zIndex);
        piece.text = text;
    } else {
        piece = self.add.sprite( 0,0, pieceInfo.assetName )
            .setOrigin(0.5, 0.5)
            .setDisplaySize(asset.sizeX, asset.sizeY);
        setPieceTexture( piece, pieceInfo );
    }
    
    piece.name = pieceInfo.name;
    pieces[piece.name] = piece;
    piece.setDepth(pieceInfo.zIndex);
    piece.gluedTo = pieceInfo.gluedTo;
    piece.setInteractive();
    
    movePiece( self, pieceInfo );
    
    if (!isSpectator) {
        piece.on('pointerover', function () {
            if ( this.gluedTo ) {
                pieces[this.gluedTo].setTint(0xe5751e);
            } else {
                this.setTint(0xe5751e);
            }
        });
        piece.on('pointerout', function () {
            if ( this.gluedTo ) {
                pieces[this.gluedTo].clearTint();
            } else {
                this.clearTint();
            }
        });
        self.input.setDraggable(piece);
    }
}

function setPieceTexture( piece, pieceInfo ) {
    // Nine patches cannot change their assetName
    if ( pieceInfo.text != null ) return;

    if ( pieceInfo.hiddenAssetNumber !== undefined ) {
        // Pick either revealAssetNumber or hiddenAssetNumber from the sprite sheet.
        if ( inRevealArea( pieceInfo ) ) {
            if (piece.faceDown) {
                piece.faceDown = false;
                playNamedSound( 'reveal' );
            }
            piece.setTexture( pieceInfo.assetName, pieceInfo.visibleAssetNumber );
        } else {
            piece.setTexture( pieceInfo.assetName, pieceInfo.hiddenAssetNumber );
            piece.faceDown = true;
        }
    } else if ( pieceInfo.hiddenAssetName ) {
        // Pick image assetName or hiddenAssetName
        if ( inRevealArea( pieceInfo ) ) {
            if (piece.faceDown) {
                piece.faceDown = false;
                playNamedSound( 'reveal' );
            }
            piece.setTexture( pieceInfo.assetName );
        } else {
            piece.setTexture( pieceInfo.hiddenAssetName );
            piece.faceDown = true
        }
    } else if ( pieceInfo.assetNumber ) {   
        piece.setTexture( pieceInfo.assetName, pieceInfo.assetNumber );
    } else {
        piece.setTexture( pieceInfo.assetName );
    }
}

function movePiece( self, moveInfo ) {
    var piece = pieces[ moveInfo.name ];
    if (!piece) return;
    
    if (moveInfo.draggingBy != null  && moveInfo.name == draggingPieceName && moveInfo.draggingBy != playerNumber) {
        // We began dragging, but another player beat us to it. So stop dragging.
        draggingPieceName = null; // Stops the dragging process.
        // TODO Make a sound?
    }
    
    if (moveInfo.ownedByPlayerNumber == null || moveInfo.ownedByPlayerNumber == playerNumber) {
        updatePieceCounters(piece.x, piece.y, moveInfo.x, moveInfo.y);
        if (piece.hidden) {
            piece.hidden = false;
            playNamedSound( 'reveal' );
        }
        piece.setPosition(moveInfo.x, moveInfo.y);
    } else {
        // The piece is owned by a different player, and if invisible to this player, so move it off screen.
        updatePieceCounters(piece.x, piece.y, -1000, -1000);
        piece.setPosition(-1000, -1000);
        piece.hidden = true;
    }

    setPieceTexture( piece, moveInfo );
    piece.setDepth(moveInfo.zIndex);
    piece.gluedTo = moveInfo.gluedTo;
    if (moveInfo.draggingBy == null || moveInfo.draggingBy == playerNumber) {
        piece.setInteractive();
    } else {
        piece.disableInteractive();
    }
    
     if ( moveInfo.text && piece.text ) {
        // A nine path with text
        //if ( moveInfo.text != piece.text.text ) {
            piece.text.setText(moveInfo.text);
            const asset = piece.asset;
            const width = max(  piece.text.width + asset.margin[1]+ asset.margin[3], asset.sizeX );
            const height = max( piece.text.height + asset.margin[0] + asset.margin[2], asset.sizeY );
            piece.resize(width,height);
        //}
        piece.text.setPosition( piece.x - piece.text.width / 2, piece.y - piece.text.height / 2 );
        piece.text.setDepth(piece.depth + 1 );
    }
    
}

/**** Chat ****/

const chatInput = document.getElementById('chatInput');
const usernameInput = document.getElementById('username');
const chatMessages = document.getElementById('chatMessages');


function appendChat( text ) {
    if (chatInput.value.length > 0 && !chatInput.value.endsWith(' ')) {
        chatInput.value = chatInput.value + ' ';
    }
    chatInput.value = chatInput.value + '@' + text;
    chatInput.focus();
}

function displayChat( sender, message, playerNumber, playSound, isSpectator ) {
    
    const messageLi = document.createElement('li');
    if (isSpectator) {
        messageLi.className = 'spectator'
    } else {
        if (playerNumber != null && playerNumber >= 0 && playerNumber < 6) {
            messageLi.className = 'player' + playerNumber;
        }
    }
    if ( sender != null ) {
        const senderSpan = document.createElement('a');
        if (playerNumber != null && playerNumber >= 0 && playerNumber < 6) {
            senderSpan.className = 'messageSender player' + playerNumber;
        } else {
            senderSpan.className = 'messageSender';
        }
        senderSpan.href = '#';
        senderSpan.onclick = function() {
            appendChat(sender);
            return false;
        };
        senderSpan.appendChild(document.createTextNode(sender));
        messageLi.appendChild(senderSpan);
    }
    
    const messageBodySpan = document.createElement('span');
    const messageBodyText = document.createTextNode(message);
    messageBodySpan.className = 'messageBody';
    messageBodySpan.appendChild(messageBodyText);
    messageLi.appendChild(messageBodySpan);

    addMessageElement(messageLi);
    if (playSound) {
        playNamedSound( message );
    }
}

function displayHTMLMessage( message ) {
    const messageLi = document.createElement('li');
    messageLi.innerHTML = message;
    
    addMessageElement(messageLi);
}

function addMessageElement(el) {
    chatMessages.appendChild(el);
    chatMessages.lastChild.scrollIntoView();
}

function playNamedSound( message ) {
    if (muted) return;
    
    var sound = sounds[ message.toLowerCase() ];
    if (Array.isArray(sound)) {
        const i = getRandomInt( sound.length );
        sound = sound[i];
    }
    if (sound) {
        sound.play();
    } else {
        sounds[ 'Ping' ][0].play();
    }
}

window.addEventListener('keydown', function(event) {
    if (event.which === 13) {
        if (document.activeElement == chatInput) {
            sendMessage();
        } else if (document.activeElement == usernameInput) {
            changeName();
            chatInput.focus();
        } else {
            chatInput.focus();
        }
    }
});

function changeNameCookie() {
    document.cookie = 'name=' + usernameInput.value;
}

function getNameCookie() {
    var name = 'name=';
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function sendMessage() {
    var message = chatInput.value;
    if (message == 'help') {
        displayHTMLMessage('<br/>' );
        displayHTMLMessage('Special chat commands :' );
        displayHTMLMessage('&nbsp;&nbsp;<b>help</b> : Displays this help text' );
        displayHTMLMessage('&nbsp;&nbsp;<b>silent</b> : Disables all chat' );
        displayHTMLMessage('&nbsp;&nbsp;<b>quiet</b> : Disables chat from spectators' );
        displayHTMLMessage('&nbsp;&nbsp;<b>loud</b> : Enables chat from players and spectators' );
        displayHTMLMessage('&nbsp;&nbsp;<b>private</b> : Ban spectators, and exclude this game from the lobby.' );
        displayHTMLMessage('&nbsp;&nbsp;<b>public</b> : Allows spectators and include this game from the lobby.' );
        displayHTMLMessage('&nbsp;&nbsp;<b>timer <i>N</i></b> : A timer for N seconds time.' );
        displayHTMLMessage('&nbsp;&nbsp;<b>timer <i>N m</i></b> : A timer for N minutes time.' );
        displayHTMLMessage('<br/>' );
        displayHTMLMessage('Button commands :' );
        for (id in buttonInfos) {
            var buttonInfo = buttonInfos[id];
            if (buttonInfo.instructions) {
                displayHTMLMessage('&nbsp;&nbsp;<b>' + id + '</b> : ' + buttonInfo.instructions);
            } else {
                displayHTMLMessage('&nbsp;&nbsp;<b>' + id + '</b>');
            }
        }
        displayHTMLMessage('<br/>' );
        displayHTMLMessage('Chat sounds (Use <b>mute</b> and <b>unmute</b> to turn sound on/off) :' );
        displayHTMLMessage('&nbsp;&nbsp;<b>yes</b>');
        displayHTMLMessage('&nbsp;&nbsp;<b>no</b>');
        displayHTMLMessage('&nbsp;&nbsp;<b>go</b>' );
        displayHTMLMessage('&nbsp;&nbsp;<b>lol</b> (Laugh)');
        displayHTMLMessage('<br/>');
        displayHTMLMessage("Hold down <b>Ctrl</b> key to highlight your mouse's position to other players." );
        displayHTMLMessage('<br/>');

    } else if (message == 'silent') {
        chatVolume = 'silent';
        displayHTMLMessage('Chat is disabled');
    } else if (message == 'quiet') {
        chatVolume = 'quiet';
        displayHTMLMessage('Spectator chat is disabled' );
    } else  if (message == 'loud' ) {
       chatVolume = 'loud';
        displayHTMLMessage('Chat is enabled (including from spectators)' );
    } else if (message == 'mute' ) {
        muted = true;
    } else if (message == 'unmute' ) {
        muted = false;
    } else if (message == 'debug') {
        debug();
    } else if (message.startsWith('timer')) {
        beginTimer( message.substring(5).trim() );
    } else if (message) {
        var button = buttonInfos[message];
        if (button) {
            scene.socket.emit('click', {name: message});
        } else {
            const re = /^[\s\d\+\-\*\/\(\)]*=$/;
            if (re.test(message)) {
                // Try to evaluate the expression
                try {
                    message = message + eval( message.substring( 0, message.length -1 ) );
                } catch(e) {
                }
            }
            
            if ( chatVolume == 'silent' ) {
                displayHTMLMessage('<b>WARNING</b>. Chat is disabled. Use the command <i>loud</i> to turn it back on.' );
            } else {
                changeNameCookie();
                scene.socket.emit('chat', { name : usernameInput.value, message : message } );
            }
        }
    }
    chatInput.value = '';
}

function beginTimer(str) {
    var scale = 1;
    if (str.endsWith('m')) {
        scale = 60;
        str.substring( 0, str.length - 1 );
    }
    var seconds = scale * parseInt(str);
    console.log( `Seconds = ${seconds} from ${str}` );
    if (isNaN(seconds)) {
        displayHTMLMessage( "Expected : <b>timer <i>N</i></b>" );
        displayHTMLMessage( "Where <i>N</i> is the number of seconds" );
        displayHTMLMessage( "You can also end with <b>m</b> for time in minutes" );
    } else {
        const func = function() { alert( "Timer finished (" + str + ")" ); };
        setTimeout( func, seconds * 1000);
        if (scale == 60) {
            displayHTMLMessage( "Added a timer for " + seconds/scale + " minutes" );
        } else {
            displayHTMLMessage( "Added a timer for " + seconds + " seconds" );
        }
    }
}

function changeName() {
    let name = usernameInput.value;
    if (name != '') {
        changeNameCookie();
        scene.socket.emit('changeName', { name : name } );
    }
}

function debug() {
    console.log( 'Piece count : ' + Object.keys(pieces).length );
    for ( var key in pieces ) {
        var piece = pieces[key];
        console.log( 'pieces[ ' + key + ' ] = { x: ' + piece.x + ', y: ' + piece.y + ' }' );
    }
    console.log( '' );
    console.log( 'PieceCounters count : ' + Object.keys(pieceCounters).length );
    for ( var key in pieceCounters ) {
        var item = pieceCounters[key];
        console.log( 'pieceCounters[ ' + key + ' ] = { x: ' + item.x + ', y: ' + item.y + ', value: ' + item.value + ' text: ' + item.text.text + ' }');
    }
    
    console.log( '' );
    console.log( 'RevealArea count : ' + Object.keys(revealAreas).length );
    for ( var key in revealAreas ) {
        var item = revealAreas[key];
        console.log( 'revealAreas[ ' + key + ' ] = { x:' + item.x + ', y: ' + item.y + ', width: ' + item.width + ', height: ' + item.height + ' }' );
    }
    console.log( '' );
    console.log( 'SnapArea count : ' + Object.keys(snapAreas).length );
    for ( var key in snapAreas ) {
        var item = snapAreas[key];
        console.log( 'snapAreas[ ' + key + ' ] = ' + debugProperties( item ) );
    }
}

function debugProperties( obj ) {
    var result = '{ ';
    for ( var id in obj ) {
        try {
            result = result + ' ' + id + ': ' + obj[id];
        } catch (e) {
            result = result + ' ' + id + ': ???';
        }
    }
    return result + ' }';
}
