// Patch missing URLSearchParams. See https://stackoverflow.com/questions/45758837/script5009-urlsearchparams-is-undefined-in-ie-11
(function (w) {
    w.URLSearchParams = w.URLSearchParams || function (searchString) {
        var self = this;
        self.searchString = searchString;
        self.get = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(self.searchString);
            if (results == null) {
                return null;
            } else {
                return decodeURI(results[1]) || 0;
            }
        };
    }
})(window)

var socket = io();
const gameTypeId = new URLSearchParams(window.location.search).get( 'gameTypeId' );

var tableBody = document.getElementById('tableBody');


var thumbnailImage = document.createElement('img');
thumbnailImage.src= '/assets/thumbnails/' + gameTypeId + '.png';
document.getElementById('thumbnailContainer').appendChild( thumbnailImage );



socket.emit( 'lobby', { gameTypeId: gameTypeId} );

socket.on('startGame', function (gameInfo) {
    document.location = '/game.html?gameId='+gameInfo.gameId;
});

socket.on('lobby', function (lobbyData) {
    
    document.getElementById('h1').innerText = lobbyData.name;
    document.title = lobbyData.name + ' Lobby';

    const variations = lobbyData.variations;
    const gameList = lobbyData.gameList;
    
    // Clear the old rows
    while (tableBody.firstChild) {
        tableBody.removeChild(tableBody.lastChild);
    }
    
    for ( var variationIndex = 0; variationIndex < variations.length; variationIndex++ ) {
        var variation = variations[variationIndex];
        addVariation( variation );
    }
    
    if ( Object.keys(gameList).length == 0 ) {
        document.getElementById('lobby').className = 'noGames';
    } else {
        document.getElementById('lobby').className = 'hasGames';
        for (var id in gameList) {
            addGameItem( gameList[id] );            
        }
    }
    
});

function addVariation( variation ) {

    const variationTd = document.createElement('td');
    variationTd.className = 'variation';
    variationTd.appendChild( document.createTextNode( variation.name ) );
    
    const gameNameInput = document.createElement('input');
    gameNameInput.type = 'text';    
    gameNameInput.placeholder = 'New game name...';
    gameNameInput.className = 'name';
    
    const nameTd = document.createElement('td');
    nameTd.className = 'name';
    nameTd.appendChild( gameNameInput );
        
    const playerCountInput = document.createElement('input');
    playerCountInput.type = 'text';
    playerCountInput.value = '2';
    
    const playerCountTd = document.createElement('td');
    playerCountTd.className = 'playerCount';
    playerCountTd.appendChild( playerCountInput );

    const lastActiveTd = document.createElement('td');
    lastActiveTd.className = 'lastActive';

    const joinButton = document.createElement('input');
    joinButton.type = 'button';
    joinButton.value = 'Start';
    joinButton.className = 'button';
    joinButton.onclick = function() { newGame( variation.variationId, gameNameInput, playerCountInput ); };
    
    const joinTd = document.createElement('td');
    joinTd.className = 'join';
    joinTd.appendChild(joinButton);
    
    const tr = document.createElement('tr');
    tr.appendChild( variationTd );
    tr.appendChild( nameTd );
    tr.appendChild( playerCountTd );
    tr.appendChild( joinTd );
    tr.appendChild( lastActiveTd );
    
    tableBody.appendChild( tr );
}

function addGameItem(gameInfo) {
    
    const variationTd = document.createElement('td');
    variationTd.className = 'variation';
    variationTd.appendChild( document.createTextNode( '' ) );
    
    const joinButton = document.createElement('input');
    joinButton.type = 'button';
    joinButton.value = 'Join';
    joinButton.className = 'button';
    joinButton.onclick = function() { document.location = '/game.html?gameId='+gameInfo.gameId };
    
    const joinTd = document.createElement('td');
    joinTd.className = 'join';
    joinTd.appendChild(joinButton);
    
    const nameTd = document.createElement('td');
    nameTd.className = 'name';
    nameTd.appendChild( document.createTextNode(gameInfo.name) );
    
    const playerCountTd = document.createElement('td');
    playerCountTd.className = 'playerCount';
    playerCountTd.appendChild( document.createTextNode('' + gameInfo.playerCount + ' of ' + gameInfo.maxPlayers) );

    const lastActiveTd = document.createElement('td');
    lastActiveTd.className = 'lastActive';
    lastActiveTd.appendChild( document.createTextNode(ago(new Date(gameInfo.lastActive))) );

    const tr = document.createElement('tr');
    tr.appendChild( variationTd );
    tr.appendChild( nameTd );
    tr.appendChild( playerCountTd );
    tr.appendChild( joinTd );
    tr.appendChild( lastActiveTd );
    
    tableBody.appendChild( tr );
}

function newGame( variationId, gameNameInput, playerCountInput ) {
    console.log( 'new game ' + gameTypeId + '(' + variationId + ') name ' + gameNameInput.value + ' playerCount ' + playerCountInput.value );
    if (gameNameInput.value != '') {
        socket.emit('newGame', {
            gameTypeId : gameTypeId,
            variationId: variationId,
            name: gameNameInput.value,
            playerCount: playerCountInput.value
        });
    } else {
        gameNameInput.focus();
    }
}

function ago( date ) {
    const seconds = Math.floor( (new Date() - date) / 1000);

    if (seconds > 60 ) {
        const minutes = Math.floor( seconds / 60 );
        if (minutes > 60) {
            const hours = Math.floor( minutes / 60 );
            if ( hours > 24 ) {
                const days = Math.floor( hours / 24 );
                return '' + days + ' day' + (days == 1 ? '' : 's') + ' ago';
            } else {
                return '' + hours + ' hour' + (hours == 1 ? '' : 's') + ' ago';
            }
        } else {
            return '' + minutes + ' minute' + (minutes == 1 ? '' : 's') + ' ago';
        }
    } else {
        return '' + seconds + ' second' + (seconds == 1 ? '' : 's') + ' ago';
    }
}

