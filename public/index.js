var socket = io();
var gamesContainer = document.getElementById('games');

socket.emit( 'listGameTypes', {} );

socket.on('gameTypes', function (gameTypes) {
    gamesContainer.innerHTML = '';
    for (var id in gameTypes) {
        addGameType( gameTypes[id] );
    }
});

function addGameType(gameTypeInfo) {
    // <a class="thumbnail" title="Cribbage" href="lobby.html?gameTypeId=cribbage"><img src="assets/thumbnails/cribbage.png" alt="Cribbage"/></a>
    
    const img = document.createElement('img');
    img.src = 'assets/thumbnails/' + gameTypeInfo.id + '.png';
    img.alt = gameTypeInfo.name;
    
    const a = document.createElement('a');
    a.className = 'thumbnail';
    a.title = gameTypeInfo.name;
    a.href= 'lobby.html?gameTypeId=' + gameTypeInfo.id;
    a.appendChild( img );
    a.appendChild( document.createElement('br') );
    a.appendChild(document.createTextNode(gameTypeInfo.name));
    a.appendChild( document.createElement('br') );
    a.appendChild(document.createTextNode('active : ' + gameTypeInfo.gameCount));

    gamesContainer.appendChild(a);
}
