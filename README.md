Play board games with friends and family across the internet.

Unlike most computer based board games, this has no rules, only pieces
that you move about as you wish. So you can cheat and have arguments
just as you would with regular board game ;-)

It was designed during the lockdown in 2020 due to the coronavirus.

Many board games can be played - you just need the right images on the server,
and a little meta-data, such as where to place the pieces at the start of the game.

Card games can be played by setting "Reveal Areas". Cards are face down outside
of Reveal Area, and face up inside them. A reveal area can be shared by everyone,
or owned by a single player.

The server uses node.js, and the client uses Phaser (an HTML5 game engine).


Status
------

It's playable, and fun to use, especially if you phone your opponent.
My family and friends have been using it quite frequently during the lockdown.

It is NOT robust.

It is really easy for dickheads to ruin games that others are playing.

No attempt has been made to stop denial of service attacks.

It's probably easy to crash the server with malformed data.

Many games are based on trust. If you want to cheat, it is easy to do so.
If you are a programmer it is easy to see other players' cards/tiles, but there
are easier ways to cheat too, such as dealing yourself extra cards/tiles.
